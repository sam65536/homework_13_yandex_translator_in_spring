public class TranslateException extends Exception {

    public TranslateException(Throwable cause) {
        super(cause);
    }
}