import source.URLSourceProvider;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Translator {
    private static final String TRANSLATION_DIRECTION = "ru";
    private static String yandexApiKey;
    private URLSourceProvider urlSourceProvider;

    public Translator() {
    }

    public void setYandexApiKey(String yandexApiKey) {
        this.yandexApiKey = yandexApiKey;
    }

    public void setUrlSourceProvider(URLSourceProvider urlSourceProvider) {
        this.urlSourceProvider = urlSourceProvider;
    }

    public String translate(String original) throws TranslateException {
        try {
            String translatedText = urlSourceProvider.load(prepareURL(original));
            return parseContent(translatedText);
        } catch (Exception e) {
            throw new TranslateException(e);
        }
    }

    private String prepareURL(String text) throws IOException {
        return "https://translate.yandex.net/api/v1.5/tr/translate?key=" + yandexApiKey
                + "&text=" + encodeText(text) + "&lang=" + TRANSLATION_DIRECTION;
    }

    private String parseContent(String content) {
        Pattern pattern = Pattern.compile("<text>(.*)</text>");
        Matcher matcher = pattern.matcher(content);
        matcher.find();
        return matcher.group(1);
    }

    private String encodeText(String text) throws IOException {
        return URLEncoder.encode(text, "UTF-8");
    }
}