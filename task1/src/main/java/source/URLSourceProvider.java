package source;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            new URL(pathToSource);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }

    @Override
    public String load(String pathToSource) throws SourceLoadingException, MalformedURLException {
        URL url = new URL(pathToSource);
        try (BufferedReader reader = new BufferedReader
                (new InputStreamReader(url.openStream())
                )
        ) {
            String currentLine;
            StringBuilder content = new StringBuilder();
            while ((currentLine = reader.readLine()) != null) {
                content.append(currentLine);
            }
            return content.toString();
        } catch (IOException e) {
            throw new SourceLoadingException(e);
        }
    }
}