import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import source.FileSourceProvider;
import source.SourceLoader;
import source.URLSourceProvider;
import java.util.Arrays;

@Configuration
@PropertySource("classpath:translator.properties")
public class AppConfig {

    @Bean
    public FileSourceProvider fileSourceProvider() {
        return new FileSourceProvider();
    }

    @Bean
    public URLSourceProvider urlSourceProvider() {
        return new URLSourceProvider();
    }

    @Bean
    public SourceLoader sourceLoader() {
        return new SourceLoader(Arrays.asList(fileSourceProvider(), urlSourceProvider()));
    }

    @Bean
    public Translator translator() {
        return new Translator(urlSourceProvider());
    }
}