package source;

import java.net.MalformedURLException;
import java.util.List;

public class SourceLoader {
    private List<SourceProvider> sourceProviders;

    public SourceLoader(List<SourceProvider> sourceProviders) {
        this.sourceProviders = sourceProviders;
    }

    public String loadSource(String pathToSource) throws SourceLoadingException, MalformedURLException {
        for (SourceProvider sourceProvider : sourceProviders) {
            if (sourceProvider.isAllowed(pathToSource)) {
                return sourceProvider.load(pathToSource);
            }
        }
        throw new SourceLoadingException();
    }
}