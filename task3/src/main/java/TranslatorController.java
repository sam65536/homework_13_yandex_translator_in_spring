import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Import;
import source.SourceConfig;
import source.SourceLoader;
import source.SourceLoadingException;
import java.net.MalformedURLException;
import java.util.Scanner;

@Import(SourceConfig.class)
public class TranslatorController {

    public static void main(String[] args) throws MalformedURLException {
        //initialization
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext();
        applicationContext.register(Translator.class, SourceConfig.class);
        applicationContext.refresh();

        SourceLoader sourceLoader = applicationContext.getBean(SourceLoader.class);
        Translator translator = applicationContext.getBean(Translator.class);

        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while(!"exit".equals(command)) {
            //TODO: add exception handling here to let user know about it and ask him to enter another path to translation
            //So, the only way to stop the application is to do that manually or type "exit"
            try {
                String source = sourceLoader.loadSource(command);
                String translation = translator.translate(source);

                System.out.println("Original: " + source);
                System.out.println("Translation: " + translation);

                command = scanner.next();
            } catch (SourceLoadingException e) {
                System.out.println(e + " enter another path...");
                command = scanner.next();
            } catch (TranslateException e) {
                System.out.println(e + " enter another path...");
                command = scanner.next();
            }
        }
    }
}