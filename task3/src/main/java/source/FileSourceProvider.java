package source;

import org.springframework.stereotype.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Component
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        File file = new File(pathToSource);
        if (file.exists() && file.canRead()) {
            return true;
        }
        return false;
    }

    @Override
    public String load(String pathToSource) throws SourceLoadingException {
        try (BufferedReader reader = new BufferedReader(new FileReader(pathToSource))) {
            String currentLine;
            StringBuilder result = new StringBuilder();
            while ((currentLine = reader.readLine()) != null) {
                result.append(currentLine);
            }
            return result.toString();
        } catch (IOException e) {
            throw new SourceLoadingException(e);
        }
    }
}