package source;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.net.MalformedURLException;
import java.util.List;

@Component
public class SourceLoader {
    @Autowired
    private List<SourceProvider> sourceProviders;

    public String loadSource(String pathToSource) throws SourceLoadingException, MalformedURLException {
        for (SourceProvider sourceProvider : sourceProviders) {
            if (sourceProvider.isAllowed(pathToSource)) {
                return sourceProvider.load(pathToSource);
            }
        }
        throw new SourceLoadingException();
    }
}