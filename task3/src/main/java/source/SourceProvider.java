package source;

import java.net.MalformedURLException;

public interface SourceProvider {

    boolean isAllowed(String pathToSource);

    String load(String pathToSource) throws SourceLoadingException, MalformedURLException;
}